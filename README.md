Command line instructions


Git global setup

git config --global user.name "Sean Chow"
git config --global user.email "seanlook7@gmail.com"

Create a new repository

git clone https://gitlab.com/seanlook/seanlok.git
cd seanlok
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/seanlook/seanlok.git
git add .
git commit
git push -u origin master